package VpsTester

//java imports
import java.io.File
import java.io.PrintWriter

//scala imports
import scala.io.Source

/**
 * Our awesome VPS tester
 */
object Main extends App {
  //trigger to run the program within a timer
  executionTimer(runProgram())

  /**
   * Timer that counts the seconds that a function runs and prints that
   *
   * @param work a function to execute
   * @tparam R anything that is executable
   * @return the result of executing work
   */
  def executionTimer[R](work: => R): R = {
    val startTime = System.nanoTime()
    val result = work
    val endTime = System.nanoTime()
    println("Elapsed time: " + (endTime - startTime) / 1000000000.0 + " seconds.")
    result
  }

  /**
   * This function is the main function that calls all the functions
   */
  def runProgram() {
    //we can make this configurable
    val files = 65000 //65K files - please dont go over full int size, Linux won't like you!
    val bytes = 13000 //13KB of text in each file

    welcome()
    removeFiles(readFiles(generateFiles(files, bytes)))
    greet()
  }

  /**
   * Welcome message
   */
  def welcome() {
    println("Welcome, VPS test is starting now")
  }

  /**
   * Function that generates a given number of with each a given size
   *
   * @param files number of files to generate
   * @param bytes size of each file
   * @return file list that is generated
   */
  def generateFiles(files: Int, bytes: Int): List[File] = {
    println("Generating files")

    val numberOfFiles = List.range(1, files)
    numberOfFiles.foreach(fileNumber => generateFile(fileNumber, bytes))

    new File(tempDir()).listFiles.filter(_.isFile).toList
  }

  /**
   * Read all the files to make sure they exist
   *
   * @param files list of files that are generated
   * @return files list of files that are read
   */
  def readFiles(files: List[File]): List[File] = {
    println("Reading files")
    files.foreach(file =>
      println(file.getName + ": " + countChars(Source.fromFile(file).mkString)) + " chars"
    )

    files
  }

  /**
   * Remove all the generated files
   *
   * @param files
   */
  def removeFiles(files: List[File]) {
    println(s"Start removing ${files.length} files")
    files.foreach(file => file.delete())
    new File(tempDir()).delete();
    println("All files removed")
  }

  /**
   * Say bye
   */
  def greet() {
    println("Done, bye!")
  }

  /**
   * File generator
   *
   * @param fileNumber the number of the file that is to be generated
   * @param bytes the size of the file in bytes that is to be generated
   */
  def generateFile(fileNumber: Int, bytes: Int) {
    writeToFile(fileNumber, getBytesOfText(bytes))
  }

  /**
   * Write a file with a given content to the system
   *
   * @param fileNumber the number of the file that is to be written
   * @param lines lines of text that needs to be passed in the file
   */
  def writeToFile(fileNumber: Int, lines: Array[String]) {
    println(s"Writing file ${fileNumber}")

    val tmpFolder = tempDir()
    val writer = new PrintWriter(
      new File(tmpFolder + File.separator + fileNumber + ".txt")
    )

    lines.foreach(line => writer.write(line + "\n"))
    writer.close()
  }

  /**
   * Return the path of our temp working dir
   * @return string
   */
  def tempDir(): String = {
    val tempFolderName = "VPStester"

    var tmpFolder = System.getProperty("java.io.tmpdir")
    tmpFolder += File.separator + tempFolderName

    val targetDirectory = new File(tmpFolder)
    if (!targetDirectory.exists()) {
      targetDirectory.mkdir()
    }

    targetDirectory.toString
  }

  /**
   * Generates a array of strings that has the size of the given bytes
   *
   * @param bytes the size the generated text should have
   * @return
   */
  def getBytesOfText(bytes: Int): Array[String] = {
    val abc = "abcdefghijklmnopqrstuvw"
    val abcChars = abc.split("(?!^)")

    val overflows = bytes / abcChars.length
    val restCount = bytes % abcChars.length

    val restString = abcChars.slice(0, restCount).mkString("")
    val strings = for (i <- 0 until overflows) yield (abc)

    strings.toArray ++ Array(restString)
  }

  /**
   * Function that counts chars in a string
   *
   * @param string
   * @return
   */
  def countChars(string: String): Int = {
    string.split("(?!^)").length
  }
}